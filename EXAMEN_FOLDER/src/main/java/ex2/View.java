package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JFrame {
    public  JButton button;
    public JTextField text1;
    public JTextField text2;

    public View() {
        button=new JButton();
        text1=new JTextField();
        text2=new JTextField();
        JPanel panel = new JPanel();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 200);
        this.setVisible(true);
        this.add(panel);
        panel.add(button);
        panel.add(text1);
        panel.add(text2);

    }

    public void addButon1Listener(ActionListener listenerForButon1) {
        button.addActionListener(listenerForButon1);
    }

}

class Controller {
    private View view;

    public Controller(View view) {
        this.view = view;
        this.view.addButon1Listener(new Listener());
    }

    class Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == view.button) {
             view.text2.setText(view.text1.getText());
            }

        }
    }
}
class App
{
    public static void main(String[] args) {
        View view=new View();
        Controller controller=new Controller(view);
    }
}




